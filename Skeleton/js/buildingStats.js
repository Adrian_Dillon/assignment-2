"use strict";
//Feature 12 and 13
let buildingBucket1 = new RoomUsageList;

//let buildingBucket = buildingBucket1.aggregateBy()[1];
let outputArea = "";
let outputAreaRef = document.getElementById("content");

delete buildingBucket._roomList

for (let prop in buildingBucket){
    if (prop !== "roomList"){
       let observationNew = buildingBucket[prop].length;
    let waste = 0;
    let seatsUsed2 = 0;
    let seatsTotal2 = 0;
    let lightsOnNew = 0;
    let heatingCoolingOnNew = 0;
    let highlighting;
    for(let i=0; i<observationNew; i++){
        if((buildingBucket[prop][i]._seatsUsed===0) & (buildingBucket[prop][i]._lightsOn || buildingBucket[prop][i]._heatingCoolingOn)){
            waste++
        }
        seatsUsed2+=buildingBucket[prop][i]._seatsUsed
        seatsTotal2+=buildingBucket[prop][i]._seatsTotal
        if(buildingBucket[prop][i]._lightsOn === true){
            lightsOnNew++
        }
        if(buildingBucket[prop][i]._heatingCoolingOn === true){
            heatingCoolingOnNew++
        }
    }
    let averageSeat
    if (seatsUsed2 === 0 || seatsTotal2 === 0){
        averageSeat = 100
    } else {
        averageSeat = (seatsUsed2/seatsTotal2*100).toFixed(1);

    }
    let averageLights = (lightsOnNew/observationNew*100).toFixed(1);
    let averageHeatingCooling = (heatingCoolingOnNew/observationNew*100).toFixed(1);
    if (waste > 0){
        
        highlighting = '<mark>' + prop + '</mark>';
    }else{
        highlighting = prop
    }
    
    outputArea += '<div class="mdl-cell mdl-cell--4-col">';
    outputArea += '<table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">';
    outputArea += '<thead><tr><th class="mdl-data-table__cell--non-numeric">';
    outputArea += '<h4>' + highlighting + '</h4></th></tr></thead>';
    outputArea += '<tbody><tr><td class="mdl-data-table__cell--non-numeric">';
    outputArea += 'Observations: ' + observationNew + '<br/>';
    outputArea += 'Wasteful observations: ' + waste + '<br/>';
    outputArea += 'Average seat utilisation: ' + averageSeat + '%<br/>';
    outputArea += 'Average lights utilisation: ' + averageLights + '%<br/>';
    outputArea += 'Average heating/cooling utilisation: ' + averageHeatingCooling + '%'
    outputArea += '</td></tr></tbody></table></div>';
      
    }
   
}

outputAreaRef.innerHTML=outputArea;
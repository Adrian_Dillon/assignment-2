"use strict";
// Feature 7 
 
const STORAGE_KEY = "ENG1003-RoomUseList"; 
 
let arrNew; 
 
var newRoomList2 = new RoomUsageList();
function retrieveArrayInLocalStorageNew(key) { 
    if (typeof (Storage) !== "undefined") { 
        arrNew = JSON.parse(localStorage.getItem(key)) 
        if (arrNew !== null) { 
            if (arrNew[arrNew.length - 1] === null) { 
                arrNew.pop(); 
            } 
        } 
    } else { 
        alert("Error: localStorage is not supported by current browser."); 
    } 
    return arrNew; 
} 
 
console.log(retrieveArrayInLocalStorageNew(STORAGE_KEY))
if (retrieveArrayInLocalStorageNew(STORAGE_KEY) !== null){
    console.log(typeof arrNew)
    for (var i in arrNew){
      console.log(arrNew[i])
    displayRooms(arrNew[i]);    
    }
} 
 
function displayRooms(arrUsedWithAllObjects) { 
    if (arrUsedWithAllObjects.length !== undefined) { 
        for (let x = arrUsedWithAllObjects.length - 1; x >= 0; x--) { 
            let lights = arrUsedWithAllObjects[x]._lightsOn; 
            if (lights === true) { 
                lights = "ON" 
            } else { 
                lights = "OFF" 
            } 
             let heater = arrUsedWithAllObjects[x]._heatingCoolingOn; 
            if (heater === true) { 
                heater = "ON" 
            } else { 
                heater = "OFF" 
            } 
            let calendar = new Date(arrUsedWithAllObjects[x]._timeChecked) 
            let months = calendar.getMonth(); 
            if (months === 0) { 
                months = "Jan" 
            } else if (months === 1) { 
                months = "Feb" 
            } else if (months === 2) { 
                months = "Mar" 
            } else if (months === 3) { 
                months = "Apr" 
             } else if (months === 4) { 
                months = "May" 
            } else if (months === 5) { 
                months = "Jun" 
            } else if (months === 6) { 
                months = "Jul" 
            } else if (months === 7) { 
                months = "Aug" 
            } else if (months === 8) { 
                months = "Sep" 
            } else if (months === 9) { 
                months = "Oct" 
            } else if (months === 10) { 
                months = "Nov" 
            } else if (months === 11) { 
                months = "Dec" 
            } 
            document.getElementById("content").innerHTML += 
                `<div class="mdl-cell mdl-cell--4-col" id = ${x}> 
                <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp"> 
                    <thead> 
                        <tr> 
                            <th class="mdl-data-table__cell--non-numeric"> 
                                <h4 class="date">${calendar.getDate()} ${months}</h4> 
                                <h4> 
                                    ${arrUsedWithAllObjects[x]._address}<br /> 
                                    Room: ${arrUsedWithAllObjects[x]._roomNumber} 
                                </h4> 
                            </th> 
                        </tr> 
                    </thead> 
                    <tbody> 
                        <tr> 
                            <td class="mdl-data-table__cell--non-numeric"> 
                                Time: ${calendar.getHours()}:${calendar.getMinutes()}:${calendar.getSeconds()}<br /> 
                                Lights: ${lights}<br /> 
                                Heating/cooling: ${heater}<br /> 
                                Seat usage: ${arrUsedWithAllObjects[x]._seatsUsed} / ${arrUsedWithAllObjects[x]._seatsTotal}<br/> 
                                <button type = "button" class="mdl-button mdl-js-button mdl-button--icon" onclick="deleteObservationAtIndex(${x})"> 
                                    <i class="material-icons">delete</i> 
                                </button> 
                            </td> 
                        </tr> 
                    </tbody> 
                </table> 
            </div>` 
        } 
    } else if (arrUsedWithAllObjects.length === 1) { 
        let lights = arrUsedWithAllObjects._lightsOn; 
        if (lights === true) { 
            lights = "ON" 
        } else { 
            lights = "OFF" 
        } 
        let heater = arrUsedWithAllObjects._heatingCoolingOn; 
        if (heater === true) { 
            heater = "ON" 
        } else { 
            heater = "OFF" 
        } 
        let calendar = new Date(arrUsedWithAllObjects._timeChecked) 
        let months = calendar.getMonth(); 
        if (months === 0) { 
            months = "Jan" 
        } else if (months === 1) { 
            months = "Feb" 
        } else if (months === 2) { 
            months = "Mar" 
        } else if (months === 3) { 
            months = "Apr" 
        } else if (months === 4) { 
            months = "May" 
        } else if (months === 5) { 
            months = "Jun" 
        } else if (months === 6) { 
            months = "Jul" 
        } else if (months === 7) { 
            months = "Aug" 
        } else if (months === 8) { 
            months = "Sep" 
        } else if (months === 9) { 
            months = "Oct" 
        } else if (months === 10) { 
            months = "Nov" 
        } else if (months === 11) { 
            months = "Dec" 
        }     
    } 
} 
  

// Feature 8    

function deleteObservationAtIndex(index){
    document.getElementById(index)
    console.log(arrNew)
    for (var i in arrNew){
        arrNew = arrNew[i]
    }
        console.log(arrNew)
        for (let a = 0; a < arrNew.length; a++){
            if ( index === a){
                arrNew.splice(a,1)
                break;
            }
        }
        
    var arrNew2 = newRoomList2.roomList
    arrNew2.push(newRoomList2.newRoom(arrNew))
//    console.log(arrNew)
    
    localStorage.setItem(STORAGE_KEY,JSON.stringify(arrNew2))
    window.location.reload()
}

// Feature 9 
 
let data = document.getElementById("content") 
let input = document.querySelector("input") 
input.oninput = search; 
// for (var i in arrNew){ 
//            arrNew = arrNew[i] 
//    } 
function search(){ 
    data.innerHTML = ""; 
    let searchArr = []; 
  
    console.log(typeof arrNew) 
    console.log( arrNew) 
    for (let i =0; i < arrNew.length; i++){ 
        if (arrNew[i]._address.toLowerCase().indexOf(searchField.value.toLowerCase()) !== -1 || 
            arrNew[i]._roomNumber.toLowerCase().indexOf(searchField.value.toLowerCase()) !== -1) { 
            searchArr.push(arrNew[i]); 
            } 
    } 
    if(searchArr.length > 0){ 
        displayRooms(searchArr) 
    } 
     
} 























"use strict";
//Feature 11
delete timeBucket._roomList

let output = "";
let outputRef = document.getElementById("content");

for (let prop in timeBucket){
    for(let i =0; i < timeBucket[prop].length; i++){
        let seatsUsedNew = timeBucket[prop][i]._seatsUsed;
        let seatsTotalNew = timeBucket[prop][i]._seatsTotal;
        let Occupancy = Number(((Number(seatsUsedNew)/Number(seatsTotalNew))*100).toFixed(1));
        console.log(Occupancy)
        if (Number(seatsTotalNew) === 0){
          timeBucket[prop][i].OccupancyInBucket= 100   
        } else {
             timeBucket[prop][i].OccupancyInBucket= Occupancy
        }
        
        if(timeBucket[prop][i]._heatingCoolingOn){
            timeBucket[prop][i]._heatingCoolingOn = 'On'
        }else{
            timeBucket[prop][i]._heatingCoolingOn = 'Off'
            
        }
        
        if(timeBucket[prop][i]._lightsOn){
            timeBucket[prop][i]._lightsOn = 'On'
        }else{
            timeBucket[prop][i]._lightsOn = 'Off'
        }
           
        let nameLength=timeBucket[prop][i]._address.length-29
        let newAddress=timeBucket[prop][i]._address
        let newTimeChecked = timeBucket[prop][i]._timeChecked
        timeBucket[prop][i]._timeChecked = newTimeChecked.substr(8,2)+' / '+newTimeChecked.substr(5,2)+' / '+newTimeChecked.substr(0,4)+' ; '+newTimeChecked.substr(11,8)
    }
    
    timeBucket[prop].sort(function(a,b){return a.OccupancyInBucket-b.OccupancyInBucket})
    console.log(timeBucket[prop])
    if (prop === "roomList"){
        
    } else {
    output+=   `<div class="mdl-cell mdl-cell--4-col">
                        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric">
                                    <h5>Worst occupancy for ${prop}</h5>
                                </th></tr>
                            </thead>`   
    }
    let limit;
    if(timeBucket[prop].length<=5){
        limit = timeBucket[prop].length
    }else{
        limit = 5;
    }
    
    let j=0
    
    while(j<limit){
        output += ` <tbody>

                              

                                <tr><td class="mdl-data-table__cell--non-numeric">
                                    <div><b>${timeBucket[prop][j]._address}; Rm ${timeBucket[prop][j]._roomNumber}</b></div>
                                    <div>Occupancy: ${timeBucket[prop][j].OccupancyInBucket}%</div>
                                    <div>Heating/cooling: ${timeBucket[prop][j]._heatingCoolingOn}</div>
                                    <div>Lights: ${timeBucket[prop][j]._lightsOn} </div>
                                    <div><font color="grey"><i> ${timeBucket[prop][j]._timeChecked}</i></div>
                                </td></tr>

                            </tbody>`
        j++
    }
    
    output += '</table></div>';
}

outputRef.innerHTML=output
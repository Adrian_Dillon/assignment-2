//Feature 1 and 2

"use strict";
class RoomUsage
    {
        constructor(roomNumber,address,lightsOn,heatingCoolingOn,seatsUsed,seatsTotal)
        {
            this._roomNumber = roomNumber;
            this._address =  address;
            this._lightsOn = lightsOn;
            this._heatingCoolingOn = heatingCoolingOn;
            this._seatsUsed = seatsUsed;
            this._seatsTotal = seatsTotal;
            let now = new Date();
            this._timeChecked = now;
        }

        //Getter Methods
        get roomNumber()
        {
            return this._roomNumber;
        }

        get address()
        {
            return this._address;
        }
        
        get lightsOn()
        {
            return this._lightsOn;
        }

        get heatingCoolingOn()
        {
            return this._heatingCoolingOn;
        }
        
        get seatsUsed()
        {
            return this._seatsUsed;
        }
        
        get seatsTotal()
        {
            return this._seatsTotal;
        }
        
        get timeChecked()
        {
            return this._timeChecked;
        }
        
        //Setter Methods
        set lightsSet(newStatus) 
        {
            if (typeof(newStatus) === "boolean")
                {
                    this._lightsOn = newStatus;
                }
        }

        set heatingCoolingSet(newStatus)
        {
            if (typeof(newStatus) === "boolean")
                {
                    this._heatingCoolingOn = newStatus;
                }
        }

        set seatsTotalSet(seatsTotal)
        {
            if ((seatsTotal) > 0 )
            {
                this._seatsTotal = seatsTotal;
            }
        }
        
        set seatsUsedSet(seatsUsed)
        {
            if ((seatsUsed) <= this.seatsTotal )
            {
                this._seatsUsed = seatsUsed;
            }
        }
    }

class RoomUsageList
    {
        constructor()
        {
            this.roomList = []
        }
        
        newRoom(room)
        {
            this.roomList.push(room);
        }
    
    
    
    
    //Feature 10    
        // Creating two buckets. One sorted with time, the other sorted according to building name
      aggregateBy (){
            
            let observations = JSON.parse(localStorage.getItem("ENG1003-RoomUseList"));
            
            timeBucket["8am"]=[];
            timeBucket["9am"]=[];
            timeBucket["10am"]=[];
            timeBucket["11am"]=[];
            timeBucket["12pm"]=[];
            timeBucket["1pm"]=[];
            timeBucket["2pm"]=[];
            timeBucket["3pm"]=[];
            timeBucket["4pm"]=[];
            timeBucket["5pm"]=[];
            timeBucket["6pm"]=[];
            
    //sorting the list in terms of hours
            for(let i=0; i < observations._roomList.length; i++){
                if(observations._roomList[i]._timeChecked.substr(11,2)=== "08"){
                   
                   timeBucket["8am"].push(observations._roomList[i]);
                
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="09"){
                     
                   timeBucket["9am"].push(observations._roomList[i]); 
            
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="10"){
                
                   timeBucket["10am"].push(observations._roomList[i]);
            
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="11"){
                  
                   timeBucket["11am"].push(observations._roomList[i]);
                
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="12"){
                
                   timeBucket["12pm"].push(observations._roomList[i]);
                
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="13"){
                
                   timeBucket["1pm"].push(observations._roomList[i]);
                
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="14"){
                
                   timeBucket["2pm"].push(observations._roomList[i]);
                
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="15"){
                
                   timeBucket["3pm"].push(observations._roomList[i]);
                 
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="16"){
                
                   timeBucket["4pm"].push(observations._roomList[i]);
                 
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="17"){
                
                   timeBucket["5pm"].push(observations._roomList[i]);
                
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="18"){
                
                   timeBucket["6pm"].push(observations._roomList[i])
            }
                
                
        let buildingName = observations._roomList[i]._address.length-29
        
        if(buildingBucket[observations._roomList[i]._address.substr(0,buildingName)]===undefined){
           buildingBucket[observations._roomList[i]._address.substr(0,buildingName)]=[]
           buildingBucket[observations._roomList[i]._address.substr(0,buildingName)].push(observations._roomList[i])
            
        }
                else{
                    buildingBucket[observations._roomList[i]._address.substr(0,buildingName)].push(observations._roomList[i])
                }
            }
        }
    }
    
                
var buildingBucket = new RoomUsageList();
var timeBucket = new RoomUsageList();

buildingBucket.aggregateBy();
timeBucket.aggregateBy();
console.log(timeBucket);
console.log(buildingBucket);


        
    

var roomList=new RoomUsageList();


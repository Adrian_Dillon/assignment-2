//Feature 3 
// Saving to local storage and clearing from observation page to add a new List 

"use strict";
const address = document.getElementById("address");
const roomNumber = document.getElementById("roomNumber");
const lights = document.getElementById("lights");
const heatingCooling = document.getElementById("heatingCooling");
const seatsTotal = document.getElementById("seatsTotal");
const seatsUsed = document.getElementById("seatsUsed");
var newRoomList = new RoomUsageList();

function saveroom()
    {
        //Conditions for each of the inputs
        if (address.value==="")
            {
                displayMessage('Please enter a valid address',3000);
            }
    
        if (roomNumber.value==="")
            {
                displayMessage('Please enter a room number',3000);
            }
    
        if (seatsTotal.value==="")
            {
                displayMessage('Please enter the total number of seats',3000);
            }
    
        if (seatsUsed.value==="")
            {
                seatsUsed.value=0
            }else if (seatsUsed.value>seatsTotal.value)
                {
                    displayMessage('Please enter the appropriate number of seats used',3000)
                }
    
        var room = new RoomUsage(roomNumber.value,address.value,lights.checked,heatingCooling.checked,seatsUsed.value,seatsTotal.value);
        var arr = newRoomList.roomList
        arr.push(newRoomList.newRoom(room))
        roomList.newRoom(room);
        localStorage.setItem('ENG1003-RoomUseList',JSON.stringify(roomList));
        console.log(roomList);
    }

function clearroom()
    {
        location.reload();
    }


// Feature 4 
// Using the geo locator API to get current location 
let navigators;

function navigatorForLocationTracking() {
    if (navigator.geolocation) {
        let positionOptions = {
            enableHighAccuracy: true,
            timeout: Infinity,
            maximumAge: 0
        };
        navigators = navigator.geolocation.watchPosition(showCurrentLocation, errorHandler, positionOptions);
    } else {
        console.log("Error in navigatorForLocationTracking");
    }
}


function errorHandler(error){
    if (error.code === 1){
        alert("Location access denied by user")
    } else if (error.code === 2){
        alert("Location unavailable")
    } else if (error.code === 3){
        alert("Location access timed out")
    } else {
        alert("Unknown error getting location")
    }
}

document.getElementById("useAddress").addEventListener("click", function(){
    if (this.checked) {
        navigatorForLocationTracking() 
    } else {
        document.getElementById("addressCheckbox").innerHTML = "Building address";
        document.getElementById("address").value = "";
    }
})


function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}


var latitude, longitude;
function showCurrentLocation(position) {
    latitude = Number(position.coords.latitude).toFixed(4);
    longitude = Number(position.coords.longitude).toFixed(4);
    autoAddress();
}

function autoAddress() {
    var apiKey = "717580d6b4c2431489c2eedd03c2f768";
    var apiUrl = "https://api.opencagedata.com/geocode/v1/json"
    var request_url = apiUrl + '?' + 'key=' + apiKey +
        '&q=' + encodeURIComponent(latitude + ',' + longitude) + '&pretty=1'
    // +'&no_annotations=1';

    var request = new XMLHttpRequest();
    request.open('GET', request_url, true);
    request.onload = function () {
    //API Response codes: https://opencagedata.com/api#codes
        if (request.status == 200) { // OK
            var data = JSON.parse(request.responseText);
            let addressFromApi = data.results[0].formatted;
            let addressUsed = "";
            // Getting address
            for (let i = 0; i < addressFromApi.length; i++) {
                if (addressFromApi[i] === ",") {
                    break;
                }
                addressUsed += addressFromApi[i];
            }
//            document.getElementById("addressCheckbox").innerHTML = "";
            document.getElementById("address").value = addressUsed;

        } else if (request.status <= 500) {
            console.log("Unable to geocode, Response Code: " + request.status + ". Check code meaning : https://opencagedata.com/api#codes ");
            
            var data = JSON.parse(request.responseText);
            console.log(data.status.message);
            
        } else {
            console.log("Internal server error");
        }
    };

    request.onerror = function () { // when there have a connection error
        console.log("Unable to connect to server!");
    };

    request.send(); // ask user for the premission of “Automatically determine my address" ?
}

//Feature 5 
// Save room list in local storage

function saveInLocalStorage (){ 
     
    function storeRoomUsage (){ 
       if (typeof(Storage)!=="undefined"){ 
            
           JSON.stringify(arrayInLocalStorage); 
           localStorage.setItem(STORAGE_KEY,JSON.stringify(arrayInLocalStorage)) 
       }else { 
           console.log("Error: localStorage is not supported by current browser.") 
       } 
    } 
   storeRoomUsage();     
} 
 
 
 
// Feature 6  
// Retrive room list from local storage in an array 
 
let arr; 
function retrieveArrayInLocalStorage(){ 
    if(typeof(Storage)!=="undefined"){ 
      //TODO: Retrieve the stored JSON string and parse 
       
     arr = JSON.parse(localStorage.getItem('ENG1003-RoomUseList')) 
     console.log(arr) 
    }else{ 
        console.log("Error: localStorage is not supported by current browser."); 
        } 
    return arr; 
} 
retrieveArrayInLocalStorage();
console.log(arr)